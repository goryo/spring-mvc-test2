package org.example.models;

public class Feature {
    private int id;
    private String name;
    private FeatureDataType dataType;
    
    public Feature(int id, String name, FeatureDataType dataType) {
        this.id = id;
        this.name = name;
        this.dataType = dataType;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public FeatureDataType getDataType() {
        return dataType;
    }
}
